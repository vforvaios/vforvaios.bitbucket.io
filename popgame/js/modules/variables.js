let canvas = document.getElementById('canvasgame')
let offsetX = canvas.offsetLeft;
let offsetY = canvas.offsetTop;
let ctx = canvas.getContext('2d')
let bubble
let bubbles = []
let min_dx = -1
let max_dx = 1
let min_dy = 1
let max_dy = 2
let quickBubbleLevel = 150
let nextBubble = 150
let scaledX = canvas.width / canvas.offsetWidth
let scaledY = canvas.height / canvas.offsetHeight
let blownbubble
let blownbubbles = []
let gravity = .85
let friction = 1.01
let blop = new Audio()
let waterSound = new Audio()
let gameoverSound = new Audio()
let success = new Audio()
let players_score = 0
let bubble_score = 20
let minRadius = 16
let maxRadius = 25
let levels = [
  { score: 800, generate: 130, spawnY: 90, min_dx: -2, max_dx: 2, min_dy: 2, max_dy: 4, pointsPerBall: 30, bg: 'underwater-2.jpg', nextFish: 250, quickNextFish: 250, nextCoin: 400, quickNextCoin: 400, coinvelocity: 14 },
  { score: 2500, generate: 110, spawnY: 85, min_dx: -3, max_dx: 3, min_dy: 3, max_dy: 5, pointsPerBall: 50, bg: 'underwater-3.jpg', nextFish: 200, quickNextFish: 200, nextCoin: 300, quickNextCoin: 300, coinvelocity: 14 },
  { score: 4500, generate: 95, spawnY: 80, min_dx: -3, max_dx: 3, min_dy: 4, max_dy: 6, pointsPerBall: 70, bg: 'underwater-6.jpg', nextFish: 180, quickNextFish: 180, nextCoin: 250, quickNextCoin: 250, coinvelocity: 14 },
  { score: 8000, generate: 85, spawnY: 75, min_dx: -4, max_dx: 4, min_dy: 5, max_dy: 7, pointsPerBall: 90, bg: 'underwater-4.jpg', nextFish: 160, quickNextFish: 160, nextCoin: 200, quickNextCoin: 200, coinvelocity: 14 },
  { score: 14000, generate: 75, spawnY: 60, min_dx: -4, max_dx: 4, min_dy: 7, max_dy: 9, pointsPerBall: 100, bg: 'underwater-5.jpg', nextFish: 140, quickNextFish: 140, nextCoin: 150, quickNextCoin: 150, coinvelocity: 14 }
]
let maxBallsMissed = 3
let database
let spawnY = 100 // distance below the canvas to spawn bubbles
let playSounds = false
let penalty_score = 10

/* FIRST FISH */
let fish = new Image()
let fish2 = new Image()
let coins = new Image()
let animatedFish
let currentframe = 4
let fishframenow
let fishframedesired
let fishframes = 7
let fishframerate = 150
let fishwidth
let fishheight
let animatedFishes = []
let nextFish = 300
let quickNextFish = 300
let fish_min_dx = 2
let fish_max_dx = 3
let randomFish
let fishType
let fish_x
let fish_y
let randomDX
let radiusFriction = 1.01
let water_ripple_vanish_radius = 22
let water_ripple
let water_ripples = []
let score_per_bubble
let scores_per_bubble = []
let levelUpIndicatorText
let levelUpIndicators = []
let min_distance = 40
let max_distance = 70
let sea_bubble
let sea_bubbles = []
let quickSeaBubble = 800
let nextSeaBubble = 800
let inARowAchievement = 7
let inARow = 0
let inARowAchievementScore = 40
let timeout
let fish_blown_bubble = false
let fish_blown_score = 100

/* COINS VARIABLES -------------------- */
let currentcoinframe = 0
let coinframenow
let coinframedesired
let coinframes = 7
let coinframerate = 100
let coinwidth = 56
let coinheight = 56
let nextCoin = 600
let quickNextCoin = 600
let coin_x
let coin_y
let animatedCoin
let animatedCoins = []
let coinSound = new Audio()
let coinScore = 70
let coinvelocity = 14